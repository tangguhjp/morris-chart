<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset=utf-8 />
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Morris.js Area Chart</title>

    <link rel="stylesheet" href="bootstrap.min.css">
    <link rel="stylesheet" href="morris/morris.css">

    <style>
        h1 {
            font-size: 50px;
            color: black;
            font-family: "fantasy";
            margin: 30px;
            padding: 10px;
        }
        label{
            font-size: 25px !important;
        }
    </style>

    <script src="jquery-1.11.1.min.js"></script>
    <script src="morris/raphael-min.js"></script>
    <script src="morris/morris.min.js"></script>

</head>
<body>

<h1 class="text-center">Data Ekspor & Impor</h1>

<div class=" text-center">
    <div id="area-chart" ></div>
    <div id="bar-chart" ></div>

</div>

    <script>
        $.ajax({
            url:'http://localhost/TekWeb/Chart/config.php',
            method: 'get',
            success: function (data) {
                chart(JSON.parse(data))
            }
        })

        function chart(data) {
            var config = {
                    data: data,
                    xkey: 'bulan',
                    ykeys: ['ekspor', 'impor'],
                    labels: ['Total Ekspor', 'Total Impor'],
                    fillOpacity: 0.3,
                    hideHover: 'auto',
                    behaveLikeLine: true,
                    resize: true,
                    parseTime: false,
                    pointFillColors:['#ffffff'],
                    pointStrokeColors: ['black'],
                    lineColors:['gray','red']
                };
            config.element = 'area-chart';
            Morris.Area(config);
            // config.element = 'line-chart';
            // Morris.Line(config);
            // config.element = 'bar-chart';
            // Morris.Bar(config);
            // config.element = 'stacked';
            // config.stacked = true;
        }
    </script>
</body>
</html>