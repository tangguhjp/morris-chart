-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 20, 2018 at 09:43 AM
-- Server version: 10.0.36-MariaDB-0ubuntu0.16.04.1
-- PHP Version: 5.6.37-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `chart`
--

-- --------------------------------------------------------

--
-- Table structure for table `ekspor_impor`
--

CREATE TABLE `ekspor_impor` (
  `id` int(11) NOT NULL,
  `bulan` text NOT NULL,
  `ekspor` double DEFAULT NULL,
  `impor` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ekspor_impor`
--

INSERT INTO `ekspor_impor` (`id`, `bulan`, `ekspor`, `impor`) VALUES
(5, 'Januari', 14553404792, 15309429258),
(6, 'Februari', 14132633647, 14185493772),
(7, 'Maret', 15586866516, 14463601047),
(8, 'April', 14537194227, 16162289358),
(9, 'Mei', 16209317934, 17662888974),
(10, 'Juni', 12974380361, 11267885237),
(11, 'Juli', 16290204390, 18297113111);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ekspor_impor`
--
ALTER TABLE `ekspor_impor`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ekspor_impor`
--
ALTER TABLE `ekspor_impor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
